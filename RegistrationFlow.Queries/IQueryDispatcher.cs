﻿using System.Threading.Tasks;

namespace RegistrationFlow.Queries
{
    public interface IQueryDispatcher
    {
        Task<TReturn> Dispatch<TReturn, TQuery>(TQuery query) where TQuery : IQuery;
    }
}