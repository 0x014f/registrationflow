﻿using Autofac;
using System.Reflection;
using Module = Autofac.Module;

namespace RegistrationFlow.Queries
{
    public class QueriesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(new[] { Assembly.GetAssembly(typeof(QueriesModule)) })
                .AsClosedTypesOf(typeof(IQueryHandler<,>));

            builder.RegisterType<QueryDispatcher>().AsImplementedInterfaces();
        }
    }
}
