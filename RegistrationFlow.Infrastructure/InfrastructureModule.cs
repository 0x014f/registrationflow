﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RegistrationFlow.Commands;
using RegistrationFlow.Infrastructure.Commands;
using RegistrationFlow.Infrastructure.Persistence;
using RegistrationFlow.Infrastructure.Persistence.Repositories;
using Module = Autofac.Module;

namespace RegistrationFlow.Infrastructure
{
    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            RegisterRepositories(builder);
            RegisterCommandDecorators(builder);

            builder.Register(c =>
            {
                var configuration = c.Resolve<IConfiguration>();
                var options = new DbContextOptionsBuilder<RegistrationFlowContext>()
                    .UseSqlServer(configuration.GetConnectionString("RegistrationFlowDatabase")).Options;
                return new RegistrationFlowContext(options);
            }).AsSelf().InstancePerLifetimeScope();

            base.Load(builder);
        }

        private static void RegisterCommandDecorators(ContainerBuilder builder)
        {
            builder.RegisterGenericDecorator(typeof(TransactionCommandHandlerDecorator<>), typeof(ICommandHandler<>));
        }

        private static void RegisterRepositories(ContainerBuilder builder)
        {
            builder.RegisterType<MrGreenCustomerRepository>().AsImplementedInterfaces();
        }
    }
}
