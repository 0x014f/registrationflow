﻿using RegistrationFlow.Commands;
using RegistrationFlow.Infrastructure.Persistence;
using System.Threading.Tasks;
using System.Transactions;

namespace RegistrationFlow.Infrastructure.Commands
{
    internal class TransactionCommandHandlerDecorator<TCommand> : ICommandHandler<TCommand> where TCommand : ICommand
    {
        private readonly ICommandHandler<TCommand> _nextCommandHandler;
        private readonly RegistrationFlowContext _registrationFlowContext;

        public TransactionCommandHandlerDecorator(
            ICommandHandler<TCommand> nextCommandHandler, 
            RegistrationFlowContext registrationFlowContext)
        {
            _nextCommandHandler = nextCommandHandler;
            _registrationFlowContext = registrationFlowContext;
        }

        public async Task Handle(TCommand command)
        {
            using var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
            await _nextCommandHandler.Handle(command);
            await _registrationFlowContext.SaveChangesAsync();
            transaction.Complete();
        }
    }
}
