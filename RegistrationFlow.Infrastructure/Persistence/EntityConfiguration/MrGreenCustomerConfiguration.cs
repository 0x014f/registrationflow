﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RegistrationFlow.Domain.Customers;

namespace RegistrationFlow.Infrastructure.Persistence.EntityConfiguration
{
    internal class MrGreenCustomerConfiguration : IEntityTypeConfiguration<MrGreenCustomer>
    {
        public void Configure(EntityTypeBuilder<MrGreenCustomer> builder)
        {
            builder.HasKey(i => i.Id);
        }
    }
}
