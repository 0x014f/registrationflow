﻿using Microsoft.EntityFrameworkCore;
using RegistrationFlow.Domain.Customers;
using System.Reflection;

namespace RegistrationFlow.Infrastructure.Persistence
{
    public class RegistrationFlowContext : DbContext
    {
        public RegistrationFlowContext(DbContextOptions<RegistrationFlowContext> options) : base(options)
        {
        }

        public DbSet<RedBetCustomer> RedBetCustomers { get; set; }
        public DbSet<MrGreenCustomer> MrGreenCustomers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var infrastructureAssembly = Assembly.GetAssembly(typeof(InfrastructureModule));
            if (infrastructureAssembly != null)
            {
                modelBuilder.ApplyConfigurationsFromAssembly(infrastructureAssembly);
            }
        }
    }
}
