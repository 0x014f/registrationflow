﻿using Microsoft.EntityFrameworkCore;
using RegistrationFlow.Domain.Customers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RegistrationFlow.Infrastructure.Persistence.Repositories
{
    internal class MrGreenCustomerRepository : IMrGreenCustomerRepository
    {
        private readonly RegistrationFlowContext _registrationFlowContext;

        public MrGreenCustomerRepository(RegistrationFlowContext registrationFlowContext)
        {
            _registrationFlowContext = registrationFlowContext;
        }

        public async Task<IEnumerable<MrGreenCustomer>> GetAll()
        {
            return await _registrationFlowContext.MrGreenCustomers.ToListAsync();
        }

        public void Add(MrGreenCustomer customer) => _registrationFlowContext.Add(customer);
    }
}
