﻿using RegistrationFlow.Domain.Common;
using System;

namespace RegistrationFlow.Domain.Customers
{
    public abstract class CustomerBase : EntityBase, IEntityWithId<Guid>
    {
        public Guid Id { get; protected set; }
        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public Address Address { get; protected set; }
        public Brand Brand { get; protected set; }
    }

    public class MrGreenCustomer : CustomerBase
    {
        public string PersonalNumber { get; protected set; }
    }

    public class RedBetCustomer : CustomerBase
    {
        public string FavoriteFootballTeam { get; protected set; }
    }

    public class Address
    {
        public string Street { get; protected set; }
        public string Number { get; protected set; }
        public string ZipCode { get; protected set; }
    }

    public abstract class Brand : IEntityWithId<Guid>
    {
        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
    }

    public class MrGreen : Brand
    {
        public new string Name => nameof(MrGreen);
    }

    public class RedBet : Brand
    {
        public new string Name => nameof(RedBet);
    }
}
