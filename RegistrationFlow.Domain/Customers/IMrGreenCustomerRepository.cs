﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace RegistrationFlow.Domain.Customers
{
    public interface IMrGreenCustomerRepository
    {
        Task<IEnumerable<MrGreenCustomer>> GetAll();
        void Add(MrGreenCustomer customer);
    }
}