﻿using System.Threading.Tasks;

namespace RegistrationFlow.Domain.Common
{
    public interface IRepository<TEntity, in TKeyType> where TEntity : EntityBase, IEntityWithId<TKeyType>
    {
        Task AddAsync(TEntity entity);
        Task RemoveAsync(TEntity entity);
        Task RemoveByIdAsync(TKeyType id);
        Task<TEntity> GetAsync(TKeyType id);
    }
}