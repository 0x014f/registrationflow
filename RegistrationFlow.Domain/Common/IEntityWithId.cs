﻿namespace RegistrationFlow.Domain.Common
{
    public interface IEntityWithId<out T>
    {
        T Id { get; }
    }
}