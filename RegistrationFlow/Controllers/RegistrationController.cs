﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RegistrationFlow.Infrastructure;
using RegistrationFlow.Requests;

namespace RegistrationFlow.Controllers
{
    [ApiController]
    [Route("register")]
    public abstract class RegistrationController<T> : ControllerBase 
        where T : RegisterCustomerRequest
    {
        private readonly ILogger<RegistrationController<T>> _logger;

        protected RegistrationController(ILogger<RegistrationController<T>> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public virtual IActionResult Register(T registrationData)
        {
            return this.Ok(typeof(T).ToString());
        }
    }
    
    [HttpHeader("X-Origin-Brand", MrGreenRegisterCustomerRequest.RequestHeader)]
    public class MrGreenRegistrationController : RegistrationController<MrGreenRegisterCustomerRequest>
    {
        public MrGreenRegistrationController(ILogger<MrGreenRegistrationController> logger) 
            : base(logger)
        {
        }
    }

    [HttpHeader("X-Origin-Brand", RedBetRegisterCustomerRequest.RequestHeader)]
    public class RebBetRegistrationController : RegistrationController<RedBetRegisterCustomerRequest>
    {
        public RebBetRegistrationController(ILogger<RebBetRegistrationController> logger)
            : base(logger)
        {
        }
    }
}
