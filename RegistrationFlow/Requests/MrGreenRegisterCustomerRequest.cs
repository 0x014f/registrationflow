﻿namespace RegistrationFlow.Requests
{
    public class MrGreenRegisterCustomerRequest : RegisterCustomerRequest
    {
        public new const string RequestHeader = "MR_GREEN";
    }
}