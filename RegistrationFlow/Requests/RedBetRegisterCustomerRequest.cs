﻿namespace RegistrationFlow.Requests
{
    public class RegisterCustomerRequest
    {
        public const string RequestHeader = "";
    }

    public class RedBetRegisterCustomerRequest : RegisterCustomerRequest
    {
        public new const string RequestHeader = "RED_BET";
    }
}