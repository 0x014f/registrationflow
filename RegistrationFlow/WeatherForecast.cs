using System;
using Autofac;
using Microsoft.AspNetCore.SignalR;
using RegistrationFlow.Commands;
using RegistrationFlow.Infrastructure;
using RegistrationFlow.Queries;

namespace RegistrationFlow
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }

    public class ApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterModule<CommandModule>();
            builder.RegisterModule<InfrastructureModule>();
            builder.RegisterModule<QueriesModule>();
        }
    }
}
