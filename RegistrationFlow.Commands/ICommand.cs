﻿using System;

namespace RegistrationFlow.Commands
{
    public interface ICommand
    {
        Guid CommandUId { get; }
    }
}