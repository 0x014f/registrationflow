﻿using System.Threading.Tasks;

namespace RegistrationFlow.Commands.Customers
{
    public class RegisterCustomerCommand : CommandBase
    {
        internal class Handler : ICommandHandler<RegisterCustomerCommand>
        {
            public Handler()
            {
            }

            public async Task Handle(RegisterCustomerCommand command)
            {
            }
        }
    }
}