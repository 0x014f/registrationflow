﻿using System.Reflection;
using Autofac;

namespace RegistrationFlow.Commands
{
    public class CommandModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(new[] { Assembly.GetAssembly(typeof(CommandModule)) })
                .AsClosedTypesOf(typeof(ICommandHandler<>));

            builder.RegisterType<CommandDispatcher>().AsImplementedInterfaces();
            base.Load(builder);
        }
    }
}