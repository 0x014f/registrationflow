﻿using System.Threading.Tasks;

namespace RegistrationFlow.Commands
{
    public interface ICommandDispatcher
    {
        Task Dispatch<TCommand>(TCommand command) where TCommand : ICommand;
    }
}