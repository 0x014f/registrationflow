﻿using System;

namespace RegistrationFlow.Commands
{
    public class CommandBase : ICommand
    {
        public Guid CommandUId { get; set; } = Guid.NewGuid();
    }
}
