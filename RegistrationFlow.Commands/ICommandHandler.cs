﻿using System.Threading.Tasks;

namespace RegistrationFlow.Commands
{
    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        Task Handle(TCommand command);
    }
}